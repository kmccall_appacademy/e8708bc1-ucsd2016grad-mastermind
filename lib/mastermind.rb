class Code
  attr_reader :pegs

  PEGS = {
    "B" => :blue,
    "G" => :green,
    "O" => :orange,
    "P" => :purple,
    "R" => :red,
    "Y" => :yellow
  }

  def initialize(pegs_arr)
    @pegs = pegs_arr
  end

  def self.parse(string)
    peg_colors = string.upcase.chars
    if peg_colors.all? {|ch| PEGS.keys.include?(ch)}
      Code.new(peg_colors)
    else
      raise 'An error has occured'
    end
  end

  def self.random
    peg_colors = PEGS.keys.sample(4)
    Code.new(peg_colors)
  end

  def [](index)
    @pegs[index]
  end

  def exact_matches(code)
    count = 0
    @pegs.each_with_index do |col, i|
      if code[i] == col
        count += 1
      end
    end
    count
  end

  def near_matches(code)
    count = 0
    counter1 = Hash.new(0)
    code.pegs.each do |col|
      counter1[col] += 1
    end
    counter2 = Hash.new(0)
    @pegs.each do |col|
      counter2[col] += 1
    end
    counter1.each do |k, v|
      if counter2.keys.include? (k)
        if v <= counter2[k]
          count += v
        else
          count += counter2[k]
        end
      end
    end

    count - self.exact_matches(code)
  end

  def ==(code)
    self.exact_matches(code) == 4
  end

end


class Game
  attr_reader :secret_code

  def initialize(code = Code.random)
    @secret_code = code
  end

  def get_guess
    puts "What is your guess?"
    input = gets.chomp
    @guess = Code.parse(input)
  end

  def display_matches(guess)
    exacts = @secret_code.exact_matches(guess)
    nears = @secret_code.near_matches(guess)
    puts "#{exacts} exact matches & #{nears} near matches"
  end

end
